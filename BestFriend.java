/**

  Title:           BestFriend PhoneBook
  Semester:        COP3804 – Fall 2017
  @author          5735374
   Instructor:     C. Charters
  
   Due Date:      10/7/2017

    
 */


package bestfriendsimulation;

import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Marie
 */
public class BestFriend 
{
    
    private static int friendCounter = 0;
    private int friendIdNumber;
    private String firstName;
    private String lastName;
    private String nickname;
    private String cellPhoneNumber;
    
    //Constructor for the attributes of each bestfriend in the phonebook
    public BestFriend(String firstName,String lastName, String nickname, String cellPhoneNumber) 
    {
        friendCounter++;
        this.friendIdNumber = friendCounter;
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickname = nickname;
        this.cellPhoneNumber = cellPhoneNumber;
    }
    
    //Constructor for the attributes of searching for a bestfriend in the phonebook
    public BestFriend(String firstName, String lastName)
    {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public int getFriendIdNumber() {
        return friendIdNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getFirstName() 
    {
        return firstName;
    }

    public void setFirstName(String firstName) 
    {
        this.firstName = firstName;
    }

    public String getNickname() 
    {
        return nickname;
    }

    public void setNickname(String nickname) 
    {
        this.nickname = nickname;
    }

    public String getCellPhoneNumber() 
    {
        return cellPhoneNumber;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) 
    {
        this.cellPhoneNumber = cellPhoneNumber;
    }
    
    // Equals method that is used to compare the users input to corresponding elements in the array
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BestFriend other = (BestFriend) obj;
        
        if (firstName.equals(other.firstName) && lastName.equals(other.lastName))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    //Displays bestfriend attributes in the phonebook
    @Override
    public String toString() {
        return "BestFriend" + "\n{friendIdNumber=" + friendIdNumber + "\nfirstName=" + firstName + "\nlastName=" + lastName + "\nnickname=" + nickname + "\ncellPhoneNumber=" + cellPhoneNumber + '}';
        
    }



    
}
